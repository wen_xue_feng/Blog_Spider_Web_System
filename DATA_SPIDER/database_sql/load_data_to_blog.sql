use blog_spider;
# 迁移数据
insert into blog(title,contents,author,category,labels,fbrq,read_num,href,source) select title,contents,author_id,category_id,labels,fbrq,read_num,href,'csdn' from csdn;
insert into blog(title,contents,author,category,labels,fbrq,source) select title,contents,author_id,category_id,labels,fbrq,'jobble' from jobble;
insert into blog(title,contents,author,category,labels,fbrq,read_num,reply_num,href,source) select title,contents,author,category,labels,fbrq,read_num,reply_num,href,'cnblogs' from cnblogs;
# 原始数据删除
# delete from csdn; # delete删除不会释放空间并且id索引还是原始的内容
truncate table csdn;# id索引从头开始计数
truncate table jobble;
truncate table cnblogs;

# 对表空间做清理
optimize table csdn;
optimize table jobble;
optimize table cnblogs;

#数据已经有了，但是爬虫会出现重复过抓取的现象，也就是说他会有重复数据
#distinct
# 删除除了重复集中最小id的其他重复记录
delete from blog where id in (select * from (select id from blog group by href having count(*)>1) as b1) and id not in (select * from (select min(id) from blog group by href having count(*)>1) as b2);
optimize table blog;